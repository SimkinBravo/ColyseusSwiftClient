//
//  ColyseusClient.swift
//
//  Created by Simkin Bravo on 01/05/18.
//  Copyright © 2018
//

import UIKit

import SwiftMsgPack
import SwiftWebSocket
import MessagePack

enum ColyseusProtocol: Int64 {

    typealias RawValue = Int64
    // User-related (0~10)
    case userId = 1
    
    // Room-related (10~20)
    case joinRoom = 10
    case joinError = 11
    case leaveRoom = 12
    case roomData = 13
    case roomState = 14
    case roomStatePatch = 15
    
    // Match-making related (20~29)
    case roomList = 20
    
    // Generic messages (50~60)
    case badRequest = 50

}

class ConnectionObject {

    var colyseusId: String!
    var socket: WebSocket!
    var queue: [[Any]] = []
    var requestId: Int = 0

    /*
     room.send({
     piece: document.querySelector('#piece').selectedIndex,
     angle: parseInt(document.querySelector('#angle').value),
     force: parseFloat(document.querySelector('#force').value)
     });
     */
    func sendMessage(_ args: [Any]) {
        var newArgs = [Any]()
        newArgs.append(contentsOf: args)
        if var options = newArgs[2] as? [String: AnyObject] {
            if colyseusId != nil {
                options["clientId"] = colyseusId as AnyObject
                newArgs[2] = options
            }
        }
        //        print(socket.readyState, newArgs)
        if socket.readyState == .open {
            var data = Data()
            do {
                print("sendMessage", newArgs)
                let enconded = try data.pack(newArgs)
                socket.send(enconded)
            } catch {
                print("Something went wrong while packing data: \(error)")
            }
        } else {
            queue.append(args)
        }
    }

}

class Room: ConnectionObject {

    var onLeave: (([AnyObject]?) -> Void)!
    var onJoin: (([AnyObject]?) -> Void)!
    var onDataChange: (([AnyObject]?) -> Void)!
    var options: [String: AnyObject]!
    var id: String!
    var name: String = ""
    var initialState: RoomStatus!
    var previousState: [UInt8] = []
    var state: [UInt8] = []

    convenience init(name: String, options: [String: AnyObject]! = [:]) {
        self.init()
        self.name = name
        self.options = options
    }

    func connect(socket: WebSocket) {
        self.socket = socket
        self.socket.event.open = {
            print("Room:WebSocket: opened")
        }
        self.socket.event.close = { code, reason, clean in
            print("Room:WebSocket: close")
        }
        self.socket.event.error = { error in
            print("Room:WebSocket: error \(error)")
        }
        self.socket.event.message = { message in
//            print("Room:WebSocket: message", message)
            if let data = message as? Data {
                do {
                    let (value, remainder) = try unpack(data)
                    if let arrayValue = value.arrayValue {
                        self.onColyseusResponse(response: arrayValue, rawResponse: data)
                    } else {
                        let response = try unpack(data)
                        print(response, remainder, "...................")
                    }
                } catch {
                    print("Something went wrong while unpacking data: \(error)")
                }
            }
        }
    }

    func leave() {
        self.socket?.close()
    }

    func send(options: [String: AnyObject]! = [:]) {
        var newOptions = options ?? [:]
        newOptions["requestId"] = requestId as AnyObject
        sendMessage([ColyseusProtocol.roomData.rawValue, id!, newOptions])
        requestId += 1
    }

    func onColyseusResponse(response: [MessagePackValue], rawResponse: Data) {
//        print("Room: onColyseusResponse:")
        if let rProtocol = ColyseusProtocol(rawValue: response[0].integerValue!) {
            switch rProtocol {
            case .userId:
                print("Room: userId", response)
            case .joinRoom:
                print("Room: joinRoom", response)
                if response[1] != nil {
                    id = response[1].stringValue
                }
                onJoin?([["status": "conectado"] as AnyObject])
//                this.onJoin.dispatch();
            case .joinError:
                print("Room: joinError", response)
            case .leaveRoom:
                print("Room: leaveRoom", response)
//                this.leave();
            case .roomData:
                print("Room: roomData", response)
//                this.onMessage.dispatch(message[1]);
            case .roomState:
                print("Room: roomState", response)
                let state = response[1].dataValue!
/*
                let remoteCurrentTime = response[2].stringValue
                let remoteElapsedTime = response[3].stringValue
*/
                let stateArray = [UInt8](state)
                setState(stateArray)
            case .roomStatePatch:
                let temp = response[1].arrayValue!
//                print("Room: roomStatePatch", temp)
                do {
                    let foo = try rawResponse.unpack() as! [AnyObject]
                    patch(binaryPatch: foo[1] as! [UInt8])
                } catch {
                    print("Something went wrong while unpacking data: \(error)")
                }
//                this.patch( message[1] );
            case .roomList:
                print("Room: roomList", response)
            case .badRequest:
                print("Room: badRequest", response)
            }
        }
    }

    func setState(_ state: [UInt8]) {
        self.previousState = self.state
        self.state = state
        let stateData = self.state.withUnsafeBufferPointer { Data(buffer: $0) }
        do {
            let stateDict = try stateData.unpack() as! [String : AnyObject]
            if initialState == nil {
                initialState = RoomStatus(response: stateDict)
            }
            onDataChange?([stateDict as AnyObject])
        } catch {
            print("Something went wrong while unpacking data: \(error)")
        }
    }

    func patch(binaryPatch: [UInt8]) {
        // apply patch
        let fd = FossilDelta()
        let temp = self.state
//        print("apply", temp, binaryPatch)
        setState(fd.apply(src: temp, delta: binaryPatch))
        // trigger state callbacks
//        this.set( msgpack.decode( this._previousState ) );
//        this.onStateChange.dispatch(this.state);
    }

}

class RoomState {

    var roomId: String!
    var clients: Int!
    var maxClients: Int!
    var metadata: Any!
    var state: Data!
    var stateArr: [UInt8]!
    var ball: CGPoint!
    var turns: Int!
    var players: [String: AnyObject]!
}

class ColyseusClient: ConnectionObject {

//    var socket: WebSocket!
    var hostname: String!
    var connectingRooms: [String: Room] = [:]
    var rooms: [String: Room] = [:]
    var roomsAvailableRequests: [String: Room] = [:]

    convenience init(url: String) {
        self.init()
        hostname = url
        setupWebSocket()
    }

    func setupWebSocket() {
        var messageNum = 0
        socket = WebSocket(hostname)
        socket.binaryType = .nsData
        socket.event.open = {
            print("opened")
        }
        socket.event.close = { code, reason, clean in
            print("close")
        }
        socket.event.error = { error in
            print("error \(error)")
        }
        socket.event.message = { message in
            if let data = message as? Data {
                do {
                    let (value, remainder) = (try unpack(data) as? (value: MessagePackValue, remainder: Data))!
                    if let arrayValue = value.arrayValue {
                        self.onColyseusResponse(response: arrayValue as! [MessagePackValue])
                    } else {
                        let response = try unpack(data)                        
                        print(response ?? "foo", "...................")
                    }
                } catch {
                    print("Something went wrong while unpacking data: \(error)")
                    self.checkQueue()
                }
            }
        }
    }

    func onColyseusResponse(response: [MessagePackValue]) {
        print("onColyseusResponse:", response)
        if let rProtocol = ColyseusProtocol(rawValue: response[0].integerValue!) {
            switch rProtocol {
            case .userId:
                print("userId", response)
                colyseusId = response[1].stringValue
            case .joinRoom:
                let requestId = String(response[2].integerValue!)
                print("joinRoom", response, requestId)
                if let room = connectingRooms[requestId] {
                    room.id = response[1].stringValue!
                    rooms[requestId] = room
                    connectingRooms[requestId] = nil
                    room.options["requestId"] = self.requestId as AnyObject
                    self.requestId += 1
                    room.connect(socket: createConnection(path: room.id, options: room.options))
                } else {
                    print("client left room before receiving session id.")
                }
            case .joinError:
                print("joinError", response)
            case .leaveRoom:
                print("leaveRoom", response)
            case .roomData:
                print("roomData", response)
            case .roomState:
                print("roomState", response)
            case .roomStatePatch:
                print("roomStatePatch")//, response)
            case .roomList:
                let temp = response[1]
                print(temp)
                let requestId = String(response[1].integerValue!)
                if roomsAvailableRequests[requestId] != nil {
                    print(response, requestId)
//                    roomsAvailableRequests[message[1]](message[2]);
                    
                } else {
//                    print("receiving ROOM_LIST after timeout:", response[2])
                }
                print("roomList", response)
            case .badRequest:
                print("badRequest", response)
            }
        }
        checkQueue()
    }

    func join(_ roomName: String, options: [String: AnyObject]! = [:]) -> Room {
        var newOptions = options ?? [:]
        newOptions["requestId"] = requestId as AnyObject
        let room = Room(name: roomName, options: newOptions)
        room.onLeave = { data in
            print(data)
//            delete this.rooms[room.id];
//            delete this.connectingRooms[options.requestId];
        }
        connectingRooms[String(requestId)] = room
        requestId += 1
        sendMessage([ColyseusProtocol.joinRoom.rawValue, roomName, newOptions])
        return room
    }

    func checkQueue() {
        if queue.count != 0 {
            let data = queue.first as! [Any]
            queue.removeFirst()
            sendMessage(data)
        }
    }

    private var completion: (([AnyObject]?) -> Void)?

    func getAvailableRooms(roomname: String,  completion: (([AnyObject]?) -> Void)! = nil) {
        self.completion = completion
        sendMessage([ColyseusProtocol.roomList.rawValue, requestId, roomname])
        requestId = requestId + 1
    }

    func createConnection(path: String, options: [String: AnyObject]! = [:]) -> WebSocket {
        var params = [String: AnyObject]()
        params["colyseusid"] = colyseusId as AnyObject
        var paramsString = "?colyseusid=\(colyseusId!)"
        for (attr, value) in options {
            if paramsString.isEmpty {
                paramsString = "?\(attr)=\(value)"
            } else {
                paramsString = paramsString + "&\(attr)=\(value)"
            }
            params[attr] = value
        }
        let url = "\(hostname!)/\(path)\(paramsString)"
        let socket = WebSocket(url)
        socket.binaryType = .nsData
        return socket
    }
}
