Quick port from https://github.com/gamestdio/colyseus.js for Swift 4

It's not fully tested. But it's working in a turn based game already published.

Some features may not function as the original client.

The code needs to be cleaned and documented, hope do it soon

StateContainer and listeners for changes on game's state are not develep, hope work it later

Use:

        client = ColyseusClient(url: "ws://blabla.com")
        room = client.join("myRoom")
        room.onJoin = { result in
            print("room.onJoin:", result!)
        }
        room.onDataChange = { data in
            if let response = data?.first as? [String: AnyObject] {
                print(response)                
            }
        }
